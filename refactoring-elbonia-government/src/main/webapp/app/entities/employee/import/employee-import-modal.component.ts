import {Component} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-employee-import-modal',
    templateUrl: './employee-import-modal.html'
})
export class EmployeeImportModalComponent {
    private file: File;

    constructor(public activeModal: NgbActiveModal) {
    }

    handleFileSelect(files: FileList) {
        if (files.length > 0) {
            this.file = files.item(0);
        }
    }

    submitFile() {
        this.activeModal.close(this.file);
    }
}
