package com.nortal.government.web.rest;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.nortal.government.domain.employee.AssociateEmployee;
import com.nortal.government.domain.employee.Employee;
import com.nortal.government.domain.employee.EmployeeSalary;
import com.nortal.government.domain.employee.ExecutiveEmployee;
import com.nortal.government.service.EmployeeSalaryService;
import com.nortal.government.service.EmployeeService;
import org.springframework.web.multipart.MultipartFile;

public class Processor {

    // arrayList containing salaries of imported employees
    private List<BigDecimal> salaries;
    // list containing names of employees with birthdays on the import date
    private List<String> birthdays;

    public Processor() {
        salaries = new ArrayList<>();
        birthdays = new ArrayList<>();
    }


    // check if it's employee's birthday
    private void checkBirthday(Employee employee) {
        if(employee.getHireDate().isEqual(LocalDate.now()))
            birthdays.add(employee.getEmployeeName());
    }

    // write executive employee to database
    private Long saveExecutiveEmployee(EmployeeService employeeService, String [] data) {
        ExecutiveEmployee executiveEmployee = new ExecutiveEmployee();
        executiveEmployee.setEmployeeName(data[0]);
        executiveEmployee.setHireDate(LocalDate.parse(data[1], DateTimeFormatter.ofPattern("yyyy-MM-d")));
        executiveEmployee.setPosition(data[3]);
        executiveEmployee.setEmail(data[5]);
        executiveEmployee.setPhoneNumber(data[6]);
        executiveEmployee.setParkingSpaceCode(data[7]);
        employeeService.save(executiveEmployee);
        checkBirthday(executiveEmployee);
        return executiveEmployee.getId();
    }

    // write associate employee to database
    private Long saveAssociateEmployee(EmployeeService employeeService, String[] data) {
        AssociateEmployee associateEmployee = new AssociateEmployee();
        associateEmployee.setEmployeeName(data[0]);
        associateEmployee.setHireDate(LocalDate.parse(data[1], DateTimeFormatter.ofPattern("yyyy-MM-d")));
        associateEmployee.setPosition(data[3]);
        associateEmployee.setFavouriteTool(data[5]);
        associateEmployee.setCoffeePreference(data[6]);
        employeeService.save(associateEmployee);
        checkBirthday(associateEmployee);
        return associateEmployee.getId();
    }

    // associate salary to employee and write it to the database
    private void saveEmployeeSalary(EmployeeSalaryService salaryService, String [] data, Long employeeId) {
        EmployeeSalary salary = new EmployeeSalary();
        salary.setEmployeeId(employeeId);
        salary.setSalary(new BigDecimal(data[4]));
        salary.setAssignDate(LocalDate.parse(data[1], DateTimeFormatter.ofPattern("yyyy-MM-d")));
        salaryService.save(salary);
        salaries.add(new BigDecimal(data[4]));
    }

    public void prRows(EmployeeService employeeService, EmployeeSalaryService salaryService, MultipartFile file) {
        BufferedReader br;
        try {
            String line;
            InputStream is = file.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            // read CSV file line by line to avoid using too much memory (in prevision for large files)
            while ((line = br.readLine()) != null) {
                String [] data = line.split(",") ;
                Long id;
                if (data[2].equals("ASSOCIATE")){
                    id = saveAssociateEmployee(employeeService, data);
                }
                else {
                    id = saveExecutiveEmployee(employeeService, data);
                }
                saveEmployeeSalary(salaryService, data, id);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<BigDecimal> getSalaries() {
        return salaries;
    }

    public List<String> getBirthday() {
        return birthdays;
    }
}
