package com.nortal.government.web.rest;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.codahale.metrics.annotation.Timed;

import com.nortal.government.domain.employee.Employee;
import com.nortal.government.repository.employee.EmployeeRepository;
import com.nortal.government.repository.employee.EmployeeSalaryRepository;
import com.nortal.government.service.EmployeeSalaryService;
import com.nortal.government.service.EmployeeService;
import com.nortal.government.web.rest.util.HeaderUtil;
import com.nortal.government.web.rest.util.PaginationUtil;

import org.apache.commons.io.IOUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/employees")
public class EmployeeResource {
    private final Logger log = LoggerFactory.getLogger(EmployeeResource.class);

    private final EmployeeService employeeService;
    // add employee salary service to write salary objects
    private final EmployeeSalaryService salaryService;

    public EmployeeResource(EmployeeService employeeService,
                            EmployeeSalaryService employeeSalaryService) {
        this.employeeService = employeeService;
        this.salaryService = employeeSalaryService;
    }

    @GetMapping
    @Timed
    public ResponseEntity<List<Employee>> getAllEmployees(Pageable pageable) {
        log.debug("REST request to get a page of Employees");
        Page<Employee> page = employeeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/employees");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/import")
    public ResponseEntity<Void> importFromFile(@RequestParam("file") MultipartFile file) {

        // Processor class refactored
        Processor processor = new Processor();
        // prRows takes a reference to the uploaded file as parameter as well as employee and salary service objects
        processor.prRows(employeeService, salaryService, file);
        Collections.sort(processor.getSalaries());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert("Import statistics: imported " + processor.getSalaries().size()+ " employees. " +
                "max imported employee salary: " + processor.getSalaries().get(processor.getSalaries().size()-1) + ", " +
                "min imported employee salary: " + processor.getSalaries().get(0) + ", " +
                "imported employee(s) having a birthday today: " + processor.getBirthday().toString().replace("[","").replace("]","")
            ))
            .build();

    }
}
