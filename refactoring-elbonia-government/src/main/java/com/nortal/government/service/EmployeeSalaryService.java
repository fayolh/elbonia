package com.nortal.government.service;

import java.util.List;
import java.util.Set;

import com.nortal.government.domain.employee.EmployeeSalary;

public interface EmployeeSalaryService {

    List<EmployeeSalary> save(List<EmployeeSalary> salaries);

    EmployeeSalary save(EmployeeSalary employeeSalary);

    EmployeeSalary findOne(Long id);

    Set<EmployeeSalary> findByEmployeeIdAndWithinLastYear(Long employeeId);

    void delete(Long id);
}
