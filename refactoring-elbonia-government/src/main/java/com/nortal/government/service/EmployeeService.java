package com.nortal.government.service;

import java.util.List;

import com.nortal.government.domain.employee.Employee;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {
    List<Employee> save(List<Employee> employees);

    Employee save(Employee employee);

    /**
     * Get all the employees.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Employee> findAll(Pageable pageable);

    Employee findOne(Long id);

    void delete(Long id);
}
