package com.nortal.expenditure;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import com.nortal.expenditure.model.Expenditure;
import org.apache.commons.lang3.tuple.Pair;

public class ExpenditureStatisticsCalculator {

    private String computeMaxFeesGainedSupplier(List<Expenditure> exp) {
        // map with keys corresponding to suppliers' names and values being fees gained
        Map<String, BigDecimal> supplierFee = new HashMap<>();
        String key = null;
        BigDecimal max = null;
        // loop through the expenditures list
        for(Expenditure e: exp) {
            // compute fee gained on the current expenditure
            BigDecimal fee = round(e.getUnits() * e.getUnitPrice() * e.getFee() * 0.01);
            // add the fee gained to the map under supplier key or update total fee gained if supplier was already in the map
            supplierFee.put(e.getSupplier(), supplierFee.getOrDefault(e.getSupplier(), new BigDecimal(0)).add(fee));
            // retrieve the supplier with max fee gained
            if( (max == null) || (max.compareTo(supplierFee.get(e.getSupplier())))< 0) {
                key = e.getSupplier();
                max = supplierFee.get(key);
            }
        }

        return key;
    }

    private BigDecimal computeAverageFee(List<Expenditure> exp) {
        // variable containing the sum of all expenses including fees
        BigDecimal totalExpenses = new BigDecimal(0L);
        totalExpenses = totalExpenses.setScale(4, RoundingMode.HALF_UP);
        // variable containing the sum of all fees
        BigDecimal totalFees = new BigDecimal(0L);
        totalFees = totalFees.setScale(4, RoundingMode.HALF_UP);
        // loop through the expenditures list
        for (Expenditure e:exp) {
            // compute fee gained on the current expenditure
            BigDecimal fee = round(e.getUnits() * e.getUnitPrice() * e.getFee() * 0.01);
            // compute the amount of expense on the current expenditure
            BigDecimal expense = round(e.getUnits() * e.getUnitPrice()).add(fee);
            // update fees total
            totalFees = totalFees.add(fee);
            // update expenses total
            totalExpenses = totalExpenses.add(expense);
        }
        // return the ratio
        return totalFees.divide(totalExpenses, RoundingMode.HALF_UP);
    }

    private Pair<String, String> computeSmallestFeeDifferenceProducts(List<Expenditure> exp) {
        // map with keys corresponding to products' names and values being fees gained
        Map<String, BigDecimal> productFee = new HashMap<>();
        // loop through the expenditures list
        for(Expenditure e: exp) {
            // compute fee gained on the current expenditure
            BigDecimal fee = round(e.getUnits() * e.getUnitPrice() * e.getFee() * 0.01);
            // add the fee gained to the map under product key or update total fee gained if product was already in the map
            productFee.put(e.getProduct(), productFee.getOrDefault(e.getProduct(), new BigDecimal(0L)).add(fee));
        }

        // convert the map into a list of entries
        List<Map.Entry<String, BigDecimal>> list = new ArrayList<>(productFee.entrySet());
        // sort the list of entries by value of the entries
        list.sort(Map.Entry.comparingByValue());

        // fee difference between the first two products in the list
        BigDecimal minDiff = list.get(1).getValue().subtract(list.get(0).getValue());
        // record names of the first two products
        String firstKey = list.get(0).getKey();
        String secondKey = list.get(1).getKey();
        // loop through remaining of the list
        for (int i = 2 ; i != list.size() ; i++) {
            // fee difference between current product and previous product in the list
            BigDecimal tmpMin = list.get(i).getValue().subtract(list.get(i-1).getValue());
            // compare the current fees difference with the previous one
            if(tmpMin.compareTo(minDiff) < 0){
                // update minimum fees difference
                minDiff = tmpMin;
                // update the two products' names
                firstKey = list.get(i-1).getKey();
                secondKey = list.get(i).getKey();
            }
        }
        // return the pair of products with smallest fees difference
        return Pair.of(firstKey, secondKey);
    }

    // rounding method
    private static BigDecimal round(double value) {
        BigDecimal bd = new BigDecimal(value);
        return bd.setScale(4, RoundingMode.HALF_UP);
    }

    public ExpenditureStatistics calcExpenditureStatistics(InputStream expenditureStream) {
        List<Expenditure> exp = new ExpenditureDataExtractor().readFromFile(expenditureStream);

        ExpenditureStatistics stats = new ExpenditureStatistics();
        stats.setMaxFeesGainedSupplier(computeMaxFeesGainedSupplier(exp));
        stats.setAvgFee(computeAverageFee(exp));
        stats.setSmallestFeeDifferenceProducts(computeSmallestFeeDifferenceProducts(exp));

        return stats;
    }

    public static class ExpenditureStatistics {
        private String maxFeesGainedSupplier;
        private BigDecimal avgFee;
        private Pair<String, String> smallestFeeDifferenceProducts;

        /**
         * @return Name of the supplier who has gained most money in additional fees
         */
        public String getMaxFeesGainedSupplier() {
            return maxFeesGainedSupplier;
        }

        private void setMaxFeesGainedSupplier(String maxFeesGainedSupplier) {
            this.maxFeesGainedSupplier = maxFeesGainedSupplier;
        }

        /**
         * @return Average fee percentage in decimal form: 34.5% = 0.345
         */
        public BigDecimal getAvgFee() {
            return avgFee;
        }

        private void setAvgFee(BigDecimal avgFee) {
            this.avgFee = avgFee;
        }

        /**
         * @return Pair of product names that have smallest difference in total paid fees
         */
        public Pair<String, String> getSmallestFeeDifferenceProducts() {
            return smallestFeeDifferenceProducts;
        }

        public void setSmallestFeeDifferenceProducts(Pair<String, String> smallestFeeDifferenceProducts) {
            this.smallestFeeDifferenceProducts = smallestFeeDifferenceProducts;
        }
    }
}
