package com.nortal.expenditure;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import com.nortal.expenditure.model.Expenditure;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExpenditureDataExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(ExpenditureDataExtractor.class);

    public List<Expenditure> readFromFile(InputStream inputStream) {

        List<Expenditure> expenditures = new ArrayList<>();

        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            Sheet sheet = workbook.getSheetAt(0);

            for(Row currentRow : sheet) {
                // skip first row (excel file header)
                if (currentRow.getRowNum() == 0)
                    continue;

                // move to next row first cell is empty (assuming that first 7 cells of each row should have data)
                Cell firstCell = currentRow.getCell(0, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                if (firstCell == null)
                    continue;

                Expenditure expenditure = new Expenditure();
                Date date = firstCell.getCellTypeEnum() == CellType.NUMERIC ?
                        firstCell.getDateCellValue() : DateUtils.parseDate(firstCell.getStringCellValue(), "dd.MM.yyyy");
                expenditure.setDate(date);
                String supplier = currentRow.getCell(1).getCellTypeEnum() == CellType.NUMERIC ?
                        "0" : currentRow.getCell(1).getStringCellValue();
                expenditure.setSupplier(supplier);
                expenditure.setType(currentRow.getCell(2).getStringCellValue());
                expenditure.setProduct(currentRow.getCell(3).getStringCellValue());
                expenditure.setUnits((int)(currentRow.getCell(4).getNumericCellValue()));
                expenditure.setUnitPrice(currentRow.getCell(5).getNumericCellValue());
                expenditure.setFee(currentRow.getCell(6).getNumericCellValue());

                expenditures.add(expenditure);
            }

        } catch (Exception e) {
            LOG.error("Error processing stream", e);
        }
        return expenditures;
    }

}
