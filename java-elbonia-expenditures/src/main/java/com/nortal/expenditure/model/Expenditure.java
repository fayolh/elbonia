package com.nortal.expenditure.model;

import java.util.Date;
import java.util.Objects;

public class Expenditure {
    private Date date;
    private String supplier;
    private String type;
    private String product;
    private int units;
    private Double unitPrice;
    private Double fee;



    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProduct() { return product; }

    public void setProduct(String product) { this.product = product; }

    public int getUnits() { return units; }

    public void setUnits(int units) { this.units = units; }

    public Double getUnitPrice() { return unitPrice; }

    public void setUnitPrice(Double unitPrice) { this.unitPrice = unitPrice; }

    public Double getFee() { return fee; }

    public void setFee(Double fee) { this.fee = fee; }

    @Override
    public String toString() {
        return this.getDate() + " " + this.getSupplier() + " " + this.getProduct() + " " + this.getType() +
                " " + this.getUnits() + " " + this.getUnitPrice() + " " + this.getFee();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expenditure that = (Expenditure) o;
        return units == that.units &&
                Objects.equals(date, that.date) &&
                Objects.equals(supplier, that.supplier) &&
                Objects.equals(type, that.type) &&
                Objects.equals(product, that.product) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(fee, that.fee);
    }

    @Override
    public int hashCode() {

        return Objects.hash(date, supplier, type, product, units, unitPrice, fee);
    }
}
