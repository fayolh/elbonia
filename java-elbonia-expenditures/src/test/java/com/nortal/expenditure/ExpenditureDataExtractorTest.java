package com.nortal.expenditure;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.ss.usermodel.*;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import com.nortal.expenditure.model.Expenditure;

public class ExpenditureDataExtractorTest {

    @Test
    public void testDataExtractionWithSimpleData() throws Exception {

        Expenditure expectedExpenditure = new Expenditure();
        expectedExpenditure.setDate(DateUtils.parseDate("14.02.2016","dd.MM.yyyy"));
        expectedExpenditure.setSupplier("Conemedia");
        expectedExpenditure.setType("Utilities");
        expectedExpenditure.setProduct("Gloves");
        expectedExpenditure.setUnits(1496);
        expectedExpenditure.setUnitPrice(10.0);
        expectedExpenditure.setFee(15.0);

        ExpenditureDataExtractor extractor = new ExpenditureDataExtractor();
        InputStream resource = ExpenditureDataExtractorTest.class.getResourceAsStream("/simple-expenditures.xlsx");
        List<Expenditure> expenditures = extractor.readFromFile(resource);

        assertThat(expenditures, contains(expectedExpenditure));
    }

    @Test
    public void testSupplierGainedMaxFeeWithTestData() throws Exception{
        // Date         Supplier    Type        Product           Units Price  Fee FeeGained
        // 29.10.2014	Abata	    Aircraft	Paper airplane	   1500	   10	15      2250
        // 19.02.2016	Abata	    Electronics	Radio Beacon	     15	 1000	25      3750
        // 1.09.2016	Blogtags	Defence	    Sandbag	           1000	  300	15     45000
        // 18.10.2013	Blogspan	Aircraft	Paper airplane     2000	   10	10      2000
        // 28.01.2015	Meevee	    Defence	    Sandbag  	       1000	  300	15     45000

        // two suppliers have gained max fees
        // expected first one to be returned - Blogtags

        InputStream resource = ExpenditureDataExtractorTest.class
                .getResourceAsStream("/two-suppliers-with-max-fees-test-expenditures.xlsx");
        ExpenditureStatisticsCalculator calc = new ExpenditureStatisticsCalculator();
        ExpenditureStatisticsCalculator.ExpenditureStatistics st = calc.calcExpenditureStatistics(resource);
        String actual = st.getMaxFeesGainedSupplier();

        resource = ExpenditureDataExtractorTest.class
                .getResourceAsStream("/two-suppliers-with-max-fees-test-expenditures.xlsx");
        List<Expenditure> expenditures = new ExpenditureDataExtractor().readFromFile(resource);
        Workbook testWorkbook = new XSSFWorkbook();
        Sheet sheet = testWorkbook.createSheet();
        for(int i=0; i<expenditures.size(); i++) {
            Row row = sheet.createRow(i);
            Cell cell = row.createCell(0);
            cell.setCellValue(expenditures.get(i).getSupplier());
            cell = row.createCell(1);
            cell.setCellValue(expenditures.get(i).getUnits()* expenditures.get(i).getUnitPrice()*expenditures.get(i).getFee()*0.01);
        }

        Cell max_position = sheet.createRow(sheet.getLastRowNum()+1).createCell(1, CellType.FORMULA);

        max_position.setCellFormula("MATCH(MAX(B1:B"+(sheet.getLastRowNum()-1)+"),B1:B"+(sheet.getLastRowNum()-1) + ", 0)");
        testWorkbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
        int pos = (int)sheet.getRow(sheet.getLastRowNum()).getCell(1).getNumericCellValue();
        String expected = sheet.getRow(pos-1).getCell(0).getStringCellValue();

        assertEquals(expected, actual);
    }

    @Test
    public void testAvgFeeWithSimpleData() {
        // Date         Supplier    Type        Product           Units Price  Fee FeeGained    Total
        // 29.10.2014	Abata	    Aircraft	Paper airplane	   1500	   10	15      2250    17250
        // 19.02.2016	Abata	    Electronics	Radio Beacon	     15	 1000	25      3750    18750
        // 1.09.2016	Meevee	    Defence	    Sandbag	           1000	  300	15     45000   345000
        // 18.10.2013	Blogspan	Aircraft	Paper airplane     2000	   10	10      2000    22000
        // 28.01.2015	Blogspan	Electronics	Extension cord  	100	  500	 5      2500    52500
        //                                                                             55000   455500

        InputStream resource = ExpenditureDataExtractorTest.class.getResourceAsStream("/test-expenditures.xlsx");
        ExpenditureStatisticsCalculator calc = new ExpenditureStatisticsCalculator();
        ExpenditureStatisticsCalculator.ExpenditureStatistics st = calc.calcExpenditureStatistics(resource);
        BigDecimal actual = st.getAvgFee();

        resource = ExpenditureDataExtractorTest.class.getResourceAsStream("/test-expenditures.xlsx");
        List<Expenditure> expenditures = new ExpenditureDataExtractor().readFromFile(resource);
        Workbook testWorkbook = new XSSFWorkbook();
        Sheet sheet = testWorkbook.createSheet();
        for(int i=0; i<expenditures.size(); i++) {
            Row row = sheet.createRow(i+1);
            Cell cell = row.createCell(0);
            double fees = expenditures.get(i).getUnits()* expenditures.get(i).getUnitPrice()*expenditures.get(i).getFee()*0.01;
            cell.setCellValue(fees);
            double total = fees + expenditures.get(i).getUnits()* expenditures.get(i).getUnitPrice();
            cell = row.createCell(1);
            cell.setCellValue(total);
        }
        Cell avg_cell = sheet.createRow(sheet.getLastRowNum()+1).createCell(0, CellType.FORMULA);
        avg_cell.setCellFormula("SUM(A1:A"+(sheet.getLastRowNum())+")/SUM(B1:B"+(sheet.getLastRowNum())+")");
        testWorkbook.getCreationHelper().createFormulaEvaluator().evaluateAll();

        BigDecimal expected = new BigDecimal(sheet.getRow(sheet.getLastRowNum()).getCell(0).getNumericCellValue());
        expected = expected.setScale(4, RoundingMode.HALF_UP);
        assertEquals(expected, actual);
    }

    @Test
    public void testProductsWithSmallestFeeDifferenceWithTestData() {
        //Product Pairs                    Fee Differences
        //Extension cord - Radio Beacon               1250
        //Radio Beacon  - Paper airplane               700
        //Paper airplane - Sandbag                   40550

        InputStream resource = ExpenditureDataExtractorTest.class.getResourceAsStream("/test-expenditures.xlsx");
        ExpenditureStatisticsCalculator calc = new ExpenditureStatisticsCalculator();
        ExpenditureStatisticsCalculator.ExpenditureStatistics st = calc.calcExpenditureStatistics(resource);

        assertEquals(Pair.of("Radio Beacon", "Paper airplane"), st.getSmallestFeeDifferenceProducts());
    }

    @Test
    public void testTwoProductsWithSmallestFeeDifferenceWithTestData() {
        //Product Pairs                    Fee Differences
        //Extension cord - Radio Beacon               1250
        //Radio Beacon  - Paper airplane              1250
        //Paper airplane - Sandbag                   40000

        List<Pair<String, String>> expectedProductPairs = new ArrayList<>();
        expectedProductPairs.add(Pair.of("Extension cord", "Radio Beacon"));
        expectedProductPairs.add(Pair.of("Radio Beacon", "Paper airplane"));

        InputStream resource = ExpenditureDataExtractorTest.class
                .getResourceAsStream("/two-products-with-smallest-fee-difference-expenditures.xlsx");
        ExpenditureStatisticsCalculator calc = new ExpenditureStatisticsCalculator();
        ExpenditureStatisticsCalculator.ExpenditureStatistics st = calc.calcExpenditureStatistics(resource);

        // First product pair with smallest fee difference should be reported
        assertEquals(expectedProductPairs.get(0), st.getSmallestFeeDifferenceProducts());
    }
}
