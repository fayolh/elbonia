package com.nortal.government.service.dto.employee;

import com.nortal.government.domain.employee.EmployeeBonus;

import java.math.BigDecimal;
import java.time.LocalDate;

// employee bonus DTO added
public class EmployeeBonusDTO {
    private Long id;
    private LocalDate bonusDate;
    private BigDecimal bonusAmount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getBonusDate() {
        return bonusDate;
    }

    public void setBonusDate(LocalDate bonusDate) {
        this.bonusDate = bonusDate;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public static EmployeeBonusDTO of(EmployeeBonus bon) {
        EmployeeBonusDTO dto = new EmployeeBonusDTO();
        dto.setId(bon.getId());
        dto.setBonusDate(bon.getBonusDate());
        dto.setBonusAmount(bon.getBonusAmount());
        return dto;
    }
}
