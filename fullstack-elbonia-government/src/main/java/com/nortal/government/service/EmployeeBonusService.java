package com.nortal.government.service;

import com.nortal.government.domain.employee.EmployeeBonus;

import java.util.Set;

// employee bonus service added to retrieve data from repository
public interface EmployeeBonusService {

    EmployeeBonus save(EmployeeBonus employeeBonus);

    EmployeeBonus findOne(Long id);

    Set<EmployeeBonus> findByEmployeeIdAndWithinLastYear(Long employeeId);

    void delete(Long id);
}
