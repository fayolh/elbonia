package com.nortal.government.repository.employee;

import com.nortal.government.domain.employee.EmployeeBonus;
import com.nortal.government.domain.employee.EmployeeSalary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Set;

/**
 * Spring Data JPA repository for the EmployeeSalary entity.
 */
// bonus repository
@SuppressWarnings("unused")
@Repository
public interface EmployeeBonusRepository extends JpaRepository<EmployeeBonus, Long> {
    // repository function to retrieve employee bonuses assigned within a year
    Set<EmployeeBonus> findByEmployeeIdAndBonusDateGreaterThanEqual(Long employeeId, LocalDate minBonusDate);
}
